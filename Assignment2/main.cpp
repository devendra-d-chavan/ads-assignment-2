#include <string>
#include <iostream>

#include "index.h"
#include "filereader.h"
#include "record_manager.h"

#define MAX_USER_INPUT_LEN 200
#define MAX_USER_INPUT_TOKENS 2

using std::cin;
using std::cout;
using std::endl;

void test_index();
void test_availability_list();
void test_record_manager();

int main( int argc, char *argv[] )
{
	if(argc != 2)
	{
		cout << "Program usage : assn_2 <db file name>. For example, assn_2 db"
				<< endl;
		return 1;
	}
	
	string db_name = (char*)argv[1];	

	string buf[MAX_USER_INPUT_TOKENS];
	bool end;
	char user_input[MAX_USER_INPUT_LEN];

	RecordManager* record_manager = new RecordManager(db_name);

	do
	{
		end = false;
		cin.getline(user_input, MAX_USER_INPUT_LEN);
		string input = user_input;

		input.token(buf, MAX_USER_INPUT_TOKENS);

		string command = buf[0];

		string argument = "";
		if(command != "end")
		{
			argument = buf[1];
		}

		if(command == "add")
		{
			record_manager->AddRecord(argument);
		}
		else if(command == "find")
		{
			record_manager->FindRecord(argument);
		}
		else if(command == "del")
		{
			record_manager->DeleteRecord(argument);
		}
		else if(command == "end")
		{
			record_manager->PrintLists();
			record_manager->WriteLists();
			end = true;
		}
		else
		{
			cout << "Invalid input";
		}
	}while(!end);
	
	delete record_manager;
	return 0;
}

void test_index()
{
	Index* index = new Index();
	index->Load("index");
	bool foo = index->Add("420885795|Kerble|Roger|Zoo");
	int x = index->FindOffset(420885795);
	bool foo1 = index->UpdateOffset(420885795, 100);
	x = index->FindOffset(420885795);
	/*long off = index->Delete(420885795);*/
	long off = index->Delete(5);
	index->Write("foo.idx");
	index->Print();
	delete index;

	index = new Index();
	index->Load("foo.idx");
	foo = index->Add("914893301|Enns|James|Psy");
	index->Print();
	delete index;
}

void test_availability_list()
{
	AvailibilityList* availibility_list = new AvailibilityList();
	availibility_list->Load("foo");

	AvailabilityListElement hole;
	hole.size = 10;
	hole.offset = 0;
	availibility_list->Add(hole);

	long offset = availibility_list->UpdateHole(8);

	hole.offset = 30;
	hole.size = 6;
	availibility_list->Add(hole);

	offset = availibility_list->UpdateHole(10);
	availibility_list->Print();

	availibility_list->Write("foo.avl");
	delete availibility_list;

	availibility_list = new AvailibilityList();
	availibility_list->Load("foo.avl");
	availibility_list->Print();
	delete availibility_list;
}

void test_record_manager()
{
	RecordManager* record_manager = new RecordManager("foo");
	/*record_manager->AddRecord("712412913|Healey|Christopher|CSC");
	record_manager->AddRecord("914893301|Enns|James|Psy");
	record_manager->AddRecord("219127719|Young|Michael|CSC");	
	record_manager->AddRecord("914893301|Enns|James|Psy");*/
	record_manager->PrintLists();

	record_manager->FindRecord("219127719");

	record_manager->DeleteRecord("219127719");
	record_manager->PrintLists();

	record_manager->AddRecord("914893341|Enns|James|Psy");	
	record_manager->PrintLists();

	record_manager->WriteLists();
	delete record_manager;
}
