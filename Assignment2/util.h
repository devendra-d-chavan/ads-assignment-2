#pragma once
#include "str.h"
#define MAX_RECORD_LENGTH 200

/// <summary>
/// Utility method
/// </summary>
class Util
{
public:	
	static int GetFileSize(string filepath);
	static int GetKeyFromRecord(string record);
};

