#pragma once
#include "str.h"
#define INITIAL_LIST_SIZE 32

/// <summary>
/// Represents a single element of the availibility list
/// </summary>
struct AvailabilityListElement {
	long offset;    // Offset of hole
	int size;		// Size of hole
};

/// <summary>
/// Represents the list of available holes in the database file
/// </summary>
class AvailibilityList
{
	AvailabilityListElement* availability_list;
	int length;
	int max_length;
	int FindFirstFitHole(int hole_size);
	
public:
	AvailibilityList(void);	
	~AvailibilityList(void);
	
	void Load(string filepath);
	void Write(string filepath);
	void Print();

	void Add(AvailabilityListElement element);
	long UpdateHole(int hole_size);
};

