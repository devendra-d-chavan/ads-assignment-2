#pragma once

#include "str.h"
#include "index.h"
#include "availibility_list.h"
#define INDEX_FILE_EXT ".idx"
#define AVL_LIST_EXT ".avl"

class filereader;

class RecordManager
{
private:
	string index_file_path;
	string availability_list_file_path;

	Index* index;
	AvailibilityList* availability_list;

	filereader* file_handler;

	void LoadLists();
public:
	RecordManager(string db_name);
	~RecordManager(void);
	
	bool AddRecord(string record);
	
	bool DeleteRecord(int key);
	bool RecordManager::DeleteRecord(string key)
	{
		return this->DeleteRecord(int(key));
	}

	bool FindRecord(int key);
	bool RecordManager::FindRecord(string key)
	{
		return this->FindRecord(int(key));
	}

	void WriteLists();		
	void PrintLists();
};

