#include <assert.h>
#include <iostream>

#include "availibility_list.h"
#include "filereader.h"
#include "util.h"

/// <summary>
/// Initializes a new instance of the <see cref="AvailibilityList"/> class.
/// </summary>
AvailibilityList::AvailibilityList(void)
{
	this->availability_list = NULL;
	this->length = -1;
}

/// <summary>
/// Finalizes an instance of the <see cref="AvailibilityList"/> class.
/// </summary>
AvailibilityList::~AvailibilityList(void)
{
	if(this->availability_list != NULL)
	{
		delete this->availability_list;
		this->availability_list = NULL;
		this->length = -1;
	}
}

/// <summary>
/// Loads the availiblity list from specified filepath.
/// </summary>
/// <param name="filepath">The path of the file containing the availibility 
/// list.</param>
void AvailibilityList::Load(string filepath)
{
	// TODO: Method always returns true
	int file_size = Util::GetFileSize(filepath);
	if(file_size <= 0)
	{
		// File does not exist, create a new index
		this->availability_list = 
			new AvailabilityListElement[INITIAL_LIST_SIZE];
		this->length = 0;
		this->max_length = INITIAL_LIST_SIZE;
		return;
	}

	// Index file exists, try to load the index
	filereader* reader =	new filereader();
	reader->open(filepath,'r', 1, 0);	

	struct AvailabilityListElement element;	
	int element_size = sizeof(element);

	this->length = file_size/element_size;
	this->max_length = this->length;
	this->availability_list = new AvailabilityListElement[this->length];

	int i = 0;
	while(!reader->eof())
	{
		if(reader->read_raw((char*)&element, element_size) == element_size)
		{
			this->availability_list[i++] = element;
		}
	}

	reader->close();
}

/// <summary>
/// Writes the availibility list to the specified filepath.
/// </summary>
/// <param name="filepath">The path of the file.</param>
void AvailibilityList::Write(string filepath)
{	
	filereader* writer = new filereader();
	writer->open(filepath, 'w', 1, 1);

	for(int i = 0; i < this->length; i++)
	{
		writer->write_raw((char*)&this->availability_list[i], 
							sizeof(this->availability_list[i]));
	}

	writer->close();
}

/// <summary>
/// Adds the specified element (hole size and offset) to the availibility list.
/// </summary>
/// <param name="hole">The hole to add.</param>
void AvailibilityList::Add(AvailabilityListElement hole)
{
	assert(this->availability_list != NULL);	
	assert(this->length != -1);

	// Check if adding the key overflows the index, if yes then resize
	if(this->length + 1 > this->max_length)
	{
		AvailabilityListElement* new_list = 
			new AvailabilityListElement[this->max_length * 2];
		this->max_length = this->max_length * 2;

		// Copy the element from the old array to the new array
		for(int i = 0; i < this->length; i++)
		{
			new_list[i] = this->availability_list[i];
		}

		// Replace the old array
		delete this->availability_list;
		this->availability_list = new_list;
	}

	// Add the hole to the end of the list
	this->availability_list[this->length] = hole;

	// Update the index length
	this->length = this->length + 1;
}

/// <summary>
/// Finds the hole of the specified size using the first fit strategy.
/// </summary>
/// <param name="hole_size">The size of the hole.</param>
/// <returns>
/// The index of the hole in the availability list if successful; 
/// -1 otherwise.
/// </returns>
int AvailibilityList::FindFirstFitHole(int hole_size)
{	
	assert(this->availability_list != NULL);
	assert(this->length != -1);

	// Check if a big enough hole exists (sequential search)
	for(int i = 0; i < this->length; i++)
	{
		if(hole_size <= this->availability_list[i].size)
		{
			// Return the index of the hole in the list
			return i;
		}
	}

	return -1;	
}

/// <summary>
/// Updates the size of the hole (filling in the hole).
/// </summary>
/// <param name="hole_size">The size of the hole.</param>
/// <returns>
/// The new offset of the hole (size is automatically decreased by the 
/// specified hole size) if successful; -1 otherwise.
/// </returns>
long AvailibilityList::UpdateHole(int hole_size)
{
	assert(this->availability_list != NULL);
	assert(this->length != -1);
	long new_offset = -1;
	int hole_index = this->FindFirstFitHole(hole_size);
	if(hole_index != -1)
	{
		// Update the hole size and offset
		this->availability_list[hole_index].size -= hole_size;
		new_offset = this->availability_list[hole_index].offset;
		this->availability_list[hole_index].offset += hole_size;
		
		if(this->availability_list[hole_index].size != 0)
		{
			AvailabilityListElement temp = this->availability_list[hole_index];
			// Move the hole to the end of the list
			for(int i = hole_index; i < this->length - 1; i++)
			{
				this->availability_list[i] = this->availability_list[i+1];
			}

			this->availability_list[this->length - 1] = temp;
		}
		else
		{
			// If the hole is full, then remove it from the list
			for(int i = hole_index; i < this->length - 1; i++)
			{
				this->availability_list[i] = this->availability_list[i+1];
			}

			this->length -= 1;

			// If the length is less that 25% of the max length then 
			// shrink the list
			if(this->length < this->max_length/4) 
			{
				AvailabilityListElement* new_list = 
					new AvailabilityListElement[this->max_length / 2];
				this->max_length = this->max_length / 2;

				// Copy the elements from the old array to the new array
				for(int i = 0; i < this->length; i++)
				{			
					new_list[i] = this->availability_list[i];
				}

				// Replace the old array
				delete this->availability_list;
				this->availability_list = new_list;
			}
		}
	}

	return new_offset;
}

/// <summary>
/// Prints the availibility list.
/// </summary>
void AvailibilityList::Print()
{
	for(int i = 0; i < this->length; i++)
	{
		std::cout << this->availability_list[i].offset << ": " 
					<< this->availability_list[i].size << std::endl;
	}
}