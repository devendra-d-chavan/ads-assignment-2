#include <assert.h>
#include <iostream>

#include "index.h"
#include "filereader.h"
#include "util.h"

/// <summary>
/// Initializes a new instance of the <see cref="Index"/> class.
/// </summary>
Index::Index(void)
{
	this->index = NULL;
	this->length = -1;
}

/// <summary>
/// Finalizes an instance of the <see cref="Index"/> class.
/// </summary>
Index::~Index(void)
{
	if(this->index != NULL)
	{
		delete this->index;
		this->index = NULL;
		this->length = -1;
	}
}

/// <summary>
/// Loads the index from the specified filepath.
/// </summary>
/// <param name="filepath">The path of the file.</param>
void Index::Load(string filepath)
{
	int file_size = Util::GetFileSize(filepath);
	if(file_size <= 0)
	{
		// File does not exist, create a new index
		this->index = new IndexElement[INITIAL_LIST_SIZE];
		this->length = 0;
		this->max_length = INITIAL_LIST_SIZE;
		return;
	}

	// Index file exists, try to load the index
	filereader* reader =	new filereader();
	reader->open(filepath,'r', 1, 0);	
	
	struct IndexElement index_element;	
	int element_size = sizeof(index_element);
	
	this->length = file_size/element_size;
	this->max_length = this->length;
	this->index = new IndexElement[this->length];

	int i = 0;
	while(!reader->eof())
	{
		if(reader->read_raw((char*)&index_element, element_size) == 
								element_size)
		{
			this->index[i++] = index_element;
		}
	}

	reader->close();
}

/// <summary>
/// Writes the index to the specified filepath.
/// </summary>
/// <param name="filepath">The path of the file.</param>
void Index::Write(string filepath)
{	
	filereader* writer = new filereader();
	writer->open(filepath, 'w', 1, 1);
	
	for(int i = 0; i < this->length; i++)
	{
		writer->write_raw((char*)&this->index[i], sizeof(this->index[i]));
	}

	writer->close();
}

/// <summary>
/// Adds the specified record to the index.
/// </summary>
/// <param name="record">The record to be added.</param>
/// <returns><c>true</c> if the record was added successfully; <c>false</c> 
/// otherwise (record exists).</returns>
bool Index::Add(string record)
{
	assert(this->index != NULL);	
	assert(this->length != -1);

	// Get the key of the record
	int key = Util::GetKeyFromRecord(record);

	assert(key != -1);
	// Check if the key already exists
	if(this->ContainsKey(key))
	{
		std::cerr << "Key exists. Cannot add existing key " << key 
					<< std::endl;
		return false;
	}

	// Check if adding the key overflows the index, if yes then resize
	if(this->length + 1 > this->max_length)
	{
		// TODO: Refactor to Expand()
		IndexElement* new_index = new IndexElement[this->max_length * 2];
		this->max_length = this->max_length * 2;

		// Copy the element from the old array to the new array
		for(int i = 0; i < this->length; i++)
		{
			new_index[i] = this->index[i];
		}

		// Replace the old array
		delete this->index;
		this->index = new_index;
	}

	int i = 0;
	while(i < this->length && key > this->index[i].key)
	{
		i++;
	}

	if(i < this->length)
	{
		// Shift the elements from i to end of the array
		for(int j = this->length; j > i; j--)
		{
			this->index[j] = this->index[j-1];
		}
	}

	this->index[i].key = key;

	// Update the index length
	this->length = this->length + 1;
	return true;
}

/// <summary>
/// Deletes the specified key from the index.
/// </summary>
/// <param name="key">The key to be deleted.</param>
/// <returns>The offset of the deleted key if successful; -1 otherwise.</returns>
long Index::Delete(int key)
{
	assert(this->index != NULL);
	assert(this->length != -1);
	int element_index = this->FindElement(key);

	if(element_index == -1)
	{
		return -1;
	}

	long offset = this->index[element_index].offset;

	// If the index is more than 75% empty, then shrink to half
	if(this->length < this->max_length/4)
	{
		IndexElement* new_index = new IndexElement[this->max_length / 2];
		this->max_length = this->max_length / 2;

		// Copy the element from the old array to the new array, 
		// but skip the element to delete
		for(int i = 0, j = 0; i < this->length; i++)
		{			
			if(this->index[i].key != key)
			{
				new_index[j++] = this->index[i];
			}			
		}

		// Replace the old array
		delete this->index;
		this->index = new_index;
	}
	else
	{
		// Shift the elements
		for(int i = element_index; i < length - 1; i++)
		{
			this->index[i] = this->index[i+1];
		}
	}

	// Update the index length
	this->length = this->length - 1;

	return offset;
}

/// <summary>
/// Finds the element with the specified key using binary search.
/// </summary>
/// <param name="key">The key.</param>
/// <returns>The index of the element if successful; -1 otherwise.</returns>
int Index::FindElement(int key)
{	
	assert(this->index != NULL);
	assert(this->length != -1);

	int min = 0;
	int max = this->length - 1;
	while (max >= min)
	{
		int mid = (min + max)/2;

		if (this->index[mid].key < key)
			min = mid + 1;
		else if (this->index[mid].key > key)
			max = mid - 1;
		else
			return mid;
	}

	return -1;	
}

/// <summary>
/// Finds the offset of the specified key.
/// </summary>
/// <param name="key">The key.</param>
/// <returns>The offset of the key if successful; -1 otherwise.</returns>
long Index::FindOffset(int key)
{
	assert(this->index != NULL);
	assert(this->length != -1);

	int element_index = this->FindElement(key);
	if(element_index == -1)
	{
		return -1;
	}

	return this->index[element_index].offset;
}

/// <summary>
/// Determines whether the specified key is present in the index.
/// </summary>
/// <param name="key">The key.</param>
/// <returns><c>true</c> if the key is present in the index; <c>false</c> 
/// otherwise.</returns>
bool Index::ContainsKey(int key)
{
	assert(this->index != NULL);
	assert(this->length != -1);

	int element_index = this->FindElement(key);
	return (element_index != -1);
}

/// <summary>
/// Updates the offset of the element with the specified key.
/// </summary>
/// <param name="key">The key.</param>
/// <param name="offset">The new offset.</param>
/// <returns><c>true</c> if the offset was updated; <c>false</c> otherwise.
/// </returns>
bool Index::UpdateOffset(int key, long offset)
{
	assert(this->index != NULL);
	assert(this->length != -1);
	assert(offset >= 0);

	int element_index = this->FindElement(key);
	if(element_index == -1)
	{
		std::cout << "Key " << key << "does not exist" << std::endl;
		return false;
	}

	this->index[element_index].offset = offset;

	return true;
}

/// <summary>
/// Prints the index.
/// </summary>
void Index::Print()
{
	for(int i = 0; i < this->length; i++)
	{
		std::cout << this->index[i].key << ": " << this->index[i].offset 
					<< std::endl;
	}
}