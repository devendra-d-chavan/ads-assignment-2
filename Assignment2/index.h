#pragma once
#include "str.h"

#define INITIAL_LIST_SIZE 32

/// <summary>
/// Represents an element in the index.
/// </summary>
struct IndexElement {
	int key;		// Key of record
	long offset;    // Offset of record in file
};

/// <summary>
/// Represents an index containing [key, offset] pairs
/// </summary>
class Index
{
private:
	IndexElement* index;
	int length;
	int max_length;

	int FindElement(int key);
public:
	Index(void);	
	~Index(void);
	
	void Load(string filepath);
	void Write(string filepath);
	void Print();

	bool Add(string record);
	long Delete(int key);
	long FindOffset(int key);
	bool ContainsKey(int key);
	bool UpdateOffset(int key, long offset);
};


