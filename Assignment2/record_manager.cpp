#include <iostream>
#include <fstream>

#include "record_manager.h"
#include "util.h"
#include "filereader.h"

/// <summary>
/// Initializes a new instance of the <see cref="RecordManager"/> class.
/// </summary>
/// <param name="db_name">The db_name.</param>
RecordManager::RecordManager(string db_name)
{
	this->index = new Index();
	this->availability_list = new AvailibilityList();

	this->index_file_path = db_name + INDEX_FILE_EXT;
	this->availability_list_file_path = db_name + AVL_LIST_EXT;

	this->LoadLists();

	file_handler = new filereader();
	file_handler->open(db_name, 'x', 0, 1);
}

/// <summary>
/// Finalizes an instance of the <see cref="RecordManager"/> class.
/// </summary>
RecordManager::~RecordManager(void)
{
	delete this->file_handler;
	delete this->index;
	delete this->availability_list;
}

/// <summary>
/// Loads the index and availiblity lists.
/// </summary>
void RecordManager::LoadLists()
{
	this->index->Load(this->index_file_path);
	this->availability_list->Load(this->availability_list_file_path);
}

/// <summary>
/// Writes the index and availiblity lists.
/// </summary>
void RecordManager::WriteLists()
{
	this->index->Write(this->index_file_path);
	this->availability_list->Write(this->availability_list_file_path);	
}

/// <summary>
/// Prints the index and availiblity lists.
/// </summary>
void RecordManager::PrintLists()
{
	std::cout << "Index:" << std::endl;
	this->index->Print();
	std::cout << "Availability:" << std::endl;
	this->availability_list->Print();
}

/// <summary>
/// Adds the record to the database.
/// </summary>
/// <param name="record">The record.</param>
/// <returns><c>true</c> if successful; <c>false</c> otherwise (duplicate key).
/// </returns>
bool RecordManager::AddRecord(string record)
{
	// Search the index for the record
	int key = Util::GetKeyFromRecord(record);
	if(this->index->ContainsKey(key))
	{
		std::cerr << "Record with SID=" << key <<" exists. Cannot add." 
					<< std::endl;
		return false;
	}

	this->index->Add(record);

	int record_length = sizeof(record_length) + record.len();
	int offset = this->availability_list->UpdateHole(record_length);

	// Find a big enough hole, if it exists
	if(offset == -1)
	{
		// Hole does not exist, move the pointer to the end of the 
		// file for the write operation
		this->file_handler->seek(0, END);
		offset = this->file_handler->offset();
	}
	else
	{
		// Hole exists, move the offset to the new location for 
		// the write operation
		this->file_handler->seek(offset, BEGIN);
	}

	// Write the contents
	this->file_handler->write_raw((char*)&record_length, 
									sizeof(record_length));
	this->file_handler->write_raw(record, 
									record_length - sizeof(record_length));

	// Update the index with the new offset
	this->index->UpdateOffset(key, offset);

	return true;
}

/// <summary>
/// Deletes the record with the specified key.
/// </summary>
/// <param name="key">The key.</param>
/// <returns><c>true</c> if successful; <c>false</c> otherwise (record not 
/// found).</returns>
bool RecordManager::DeleteRecord(int key)
{
	long offset = this->index->FindOffset(key);
	if(offset == -1)
	{
		std::cout << "No record with SID=" << key <<" exists" << std::endl;
		return false;
	}
	
	this->file_handler->seek(offset, BEGIN);
	int record_length;
	this->file_handler->read_raw((char*)&record_length, sizeof(record_length));

	AvailabilityListElement element;
	element.offset = offset;
	element.size = record_length;

	this->availability_list->Add(element);
	
	this->index->Delete(key);

	return true;
}

/// <summary>
/// Finds and prints the record with the specified key.
/// </summary>
/// <param name="key">The key.</param>
/// <returns><c>true</c> if the record is found; <c>false</c> otherwise.
/// </returns>
bool RecordManager::FindRecord(int key)
{
	long offset = this->index->FindOffset(key);
	if(offset == -1)
	{
		std::cout << "No record with SID=" << key << " exists" << std::endl;
		return false;
	}

	this->file_handler->seek(offset, BEGIN);
	int record_length;
	char buf[MAX_RECORD_LENGTH];
	for(int i = 0; i < MAX_RECORD_LENGTH; i++)
	{
		buf[i] = '\0';
	}

	this->file_handler->read_raw((char*)&record_length, sizeof(record_length));
	this->file_handler->read_raw(buf, record_length - sizeof(record_length));	

	std::cout << buf << std::endl;

	return true;
}


