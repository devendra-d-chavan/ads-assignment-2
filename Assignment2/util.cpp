#include "util.h"
#include <fstream>

/// <summary>
/// Gets the size of the file.
/// </summary>
/// <param name="filepath">The filepath.</param>
/// <returns>The size of the file if successful; -1 otherwise.</returns>
int Util::GetFileSize(string filepath)
{
	std::ifstream fp;       // Input file stream

	fp.open( filepath, std::ios::binary | std::ios::ate );
	if(!fp.is_open())
	{
		return -1;
	}

	int length = fp.tellg();	
	fp.close();	

	return length;
}

/// <summary>
/// Retrieves the key from record.
/// </summary>
/// <param name="record">The record.</param>
/// <returns>The value of key if successful; -1 otherwise.</returns>
int Util::GetKeyFromRecord(string record)
{
	string buf[MAX_RECORD_LENGTH];
	if(record.token(buf, MAX_RECORD_LENGTH, "|") > 0 &&
		buf[0].is_int())
	{
		return int(buf[0]);
	}

	return -1;
}

