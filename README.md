CSC 541: Assignment #2 - In-Memory Indexing w/Availability Lists
=================================================================
The goals of this assignment are three-fold:

 1. To investigate the use of field delimiters and record counts for field and record organization.
 2. To build and maintain an in-memory primary key index to improve search efficiency.
 3. To use an in-memory availability list to support the reallocation of space for records that are deleted.
 
Environment
---------------------
Visual Studio Ultimate on Windows 7 64 bit. Build targeted for Windows XP 32 bit.